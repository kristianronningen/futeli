# futeli

FotoUtfordring TEmaLIste

Svært enkelt system for fotoklubber for å holde jevnlige (personlige)
foto-utfordringer med forskjellige temaer.

## Konfigurering

Kopier `config-sample.php` til `config.php` og oppdater den med korrekt info for
database-tilkoblingen.

## Database

Se `futeli.sql`.

Hvis du vil ha en "kickstart" med temaer, se `themes.sql`.

## Beskytt administrerings-siden

Det er ingen innbygd innloggings-mekanisme i futeli, så bruk en .htaccess-fil
for passord-beskyttelse av `admin/`-katalogen, med tilhørende .htpasswd-fil. Det
finnes masse guider der ute om hvordan dette kan settes opp.
