<?php

require_once('../config.php');
require_once('../futeli.php');

// print("<pre>");
// print_r($_POST);
// print("</pre>");

$objFuteli = new Futeli($strDbHost, $strDbUser, $strDbPass, $strDbName);

if (isset($_POST['submit']) && $_POST['submit'] == 'Lag ny utfordring!' && strlen($_POST['newEventName']) > 0) {
    $objFuteli->createEvent($_POST['newEventName']);
}

if (isset($_POST['submit']) && $_POST['submit'] == 'Slett utfordring') {
    $objFuteli->deleteEvent($_POST['eventid']);
}
if (isset($_POST['submit']) && $_POST['submit'] == 'Nytt tema') {
    $objFuteli->pickRandomSingleTheme($_POST['eventid'], $_POST['participantid']);
}

if (isset($_POST['submit']) && $_POST['submit'] == 'Lås temaer') {
    $objFuteli->lockEvent($_POST['eventid']);
}

if (isset($_POST['submit']) && $_POST['submit'] == 'Lås opp temaer') {
    $objFuteli->unlockEvent($_POST['eventid']);
}

if (isset($_POST['submit']) && $_POST['submit'] == 'Deaktiver' && isset($_POST['participantid'])) {
    $objFuteli->deactivateParticipant($_POST['participantid']);
}

if (isset($_POST['submit']) && $_POST['submit'] == 'Aktiver' && isset($_POST['participantid'])) {
    $objFuteli->activateParticipant($_POST['participantid']);
}

if (isset($_POST['submit']) && $_POST['submit'] == 'Legg til ny deltager') {
    $objFuteli->addParticipant($_POST['participantname']);
}

if (isset($_POST['submit']) && $_POST['submit'] == 'Deaktiver' && isset($_POST['themeid'])) {
    $objFuteli->deactivateTheme($_POST['themeid']);
}

if (isset($_POST['submit']) && $_POST['submit'] == 'Aktiver' && isset($_POST['themeid'])) {
    $objFuteli->activateTheme($_POST['themeid']);
}

if (isset($_POST['submit']) && $_POST['submit'] == 'Legg til nytt tema') {
    $objFuteli->addTheme($_POST['themename']);
}

header("Location: " . dirname($_SERVER['SCRIPT_NAME']) . "/");
