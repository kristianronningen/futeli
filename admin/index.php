<?php

require_once('../config.php');
require_once('../futeli.php');

print("<!DOCTYPE html>
<html>
<head>
    <meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\" />
    <link rel=\"stylesheet\" type=\"text/css\" href=\"../futeli.css\" />
    <title>Utfordringen - Administrering</title>
</head>
<body>");

// Show button for creating new event
print('
<h3>Ny utfordring</h3>
<form accept-charset="UTF-8" method="POST" action="action.php">
<table border="0">
    <tbody>
    <tr>
        <td>Navn:</td>
        <td><input class="ny" type="text" name="newEventName" size="30" maxlength="50" /></td>
    </tr>
    <tr>
        <td colspan="2"><input class="ny" type="submit" name="submit" value="Lag ny utfordring!" /></td>
    </tr>
    </tbody>
</table>
</form>
');

$objFuteli = new Futeli($strDbHost, $strDbUser, $strDbPass, $strDbName);
$arrEvents = $objFuteli->getEvents();

foreach ($arrEvents as $eventId => $arrEventInfo) {
    // For each event, fetch the pairings (participant<->theme)
    $arrEventPairings[$eventId] = $objFuteli->getEventPairings($eventId);
}

// Build a table of the events, participants and themes
print('<hr><h3>Utfordringer</h3>
<table border="0">
    <thead>
    <tr>');
// Header, one column for each event
foreach ($arrEvents as $eventId => $arrEventInfo) {
    // Convert the draw date to a more human readable form
    $intTimestamp = strtotime($arrEventInfo['created']);
    $fmt = datefmt_create('nb_NO.UTF-8', IntlDateFormatter::FULL, IntlDateFormatter::FULL, 'Europe/Oslo', IntlDateFormatter::GREGORIAN  ,"d. LLL yyyy");
    print('
        <th' . $objFuteli->lockedStyle($eventId) . '>' . $arrEventInfo['name'] . '<br />
            <span class="trukket">Trukket: ' . datefmt_format($fmt, $intTimestamp) . '</span>');
    if ($objFuteli->isEventLocked($eventId) === false) {
        print('
            <form accept-charset="UTF-8" method="POST" action="action.php">
            <input type="hidden" name="eventid" value="' . $eventId . '" />
            <input class="warning" type="submit" name="submit" value="Lås temaer" />
            <input class="warning" type="submit" name="submit" value="Slett utfordring" />
            </form>');
    }
    else {
        print('
            <form accept-charset="UTF-8" method="POST" action="action.php">
            <input type="hidden" name="eventid" value="' . $eventId . '" />
            <input type="submit" name="submit" value="Lås opp temaer" />
            <input class="warning" type="submit" name="submit" value="Slett utfordring" />
            </form>');
    }
    print('
        </th>');
}
print("
    </tr>
    </thead>
    <tbody>
    <tr>");
// @file_put_contents("/home/steinbru/utfordringen-debug.log", "arrEventPairings: " . var_export($arrEventPairings, true) . "\n", FILE_APPEND);
foreach($arrEventPairings as $eventId => $arrPairingInfo) {
    print('
        <td valign="top"' . $objFuteli->lockedStyle($eventId) . '>
            <table>
            <tbody>');
    foreach ($arrPairingInfo as $participantId => $themeId) {
        if ($objFuteli->isParticipantActive($participantId)) {
            print('
                    <tr>
                        <td>' . $objFuteli->getParticipantName($participantId) . "</td>
                        <td>" . $objFuteli->getThemeName($themeId) . "</td>");
            if (!$objFuteli->isEventLocked($eventId)) {
                print('
                        <td>
                            <form accept-charset="UTF-8" method="POST" action="action.php">
                            <input type="hidden" name="eventid" value="' . $eventId . '" />
                            <input type="hidden" name="participantid" value="' . $participantId . '" />
                            <input type="submit" name="submit" value="Nytt tema" />
                            </form>
                        </td>');
            }
            print("
                    </tr>");
        }
    }
    print("
            </tbody>
            </table>
        </td>");
}

print("
    </tr>
    </tbody>
</table>");


// Show a list of participants, allow to add new, and deactivate existing
print('<hr><h3>Deltagere</h3>
<p>' . $objFuteli->getActiveParticipantCount() . ' aktive, ' . $objFuteli->getInactiveParticipantCount() . ' inaktive, ' . $objFuteli->getParticipantCount() . ' totalt.</p>
<table border="0">
    <thead>
    <tr>
        <th>Navn</th>
        <th>#</th>
        <th>Handling</th>
    </tr>
    </thead>
    <tbody>');

foreach($objFuteli->getParticipants() as $intParticipantId => $arrParticipantInfo) {
    print('
        <tr>
            <td ' . ($objFuteli->isParticipantActive($intParticipantId) ? 'class="active"' : 'class="inactive"') . '>' . $arrParticipantInfo['name'] . '</td>
            <td ' . ($objFuteli->isParticipantActive($intParticipantId) ? 'class="active"' : 'class="inactive"') . '>' . $objFuteli->getParticipantPairingCount($intParticipantId) . '</td>
            <td>
                <form accept-charset="UTF-8" method="POST" action="action.php">
                <input type="hidden" name="participantid" value="' . $intParticipantId . '" />');
    if ($objFuteli->isParticipantActive($intParticipantId)) {
        print('
                <input type="submit" name="submit" value="Deaktiver" />');
    }
    else {
        print('
                <input type="submit" name="submit" value="Aktiver" />');
    }
    print('
                </form>
            </td>
        </tr>');
}
print('
        <tr>
            <td colspan="3">
                <form accept-charset="UTF-8" method="POST" action="action.php">
                <input type="text" name="participantname" size="12">
                <input type="submit" name="submit" value="Legg til ny deltager" />
                </form>
            </td>
        </tr>
    </tbody>
</table>');

// Show a list of themes, allow to add new, and deactivate existing
print('<hr><h3>Temaer</h3>
<p>' . $objFuteli->getActiveThemeCount() . ' aktive, ' . $objFuteli->getInactiveThemeCount() . ' inaktive, ' . $objFuteli->getThemeCount() . ' totalt.</p>
<table border="0">
    <thead>
    <tr>
        <th>Tema</th>
        <th>#</th>
        <th>Handling</th>
        <th>Tema</th>
        <th>#</th>
        <th>Handling</th>
        <th>Tema</th>
        <th>#</th>
        <th>Handling</th>
    </tr>
    </thead>
    <tbody>');

$intLoopCount = 0;
foreach($objFuteli->getThemes() as $intThemeId => $arrThemeInfo) {
    if ($intLoopCount++ % 3 == 0) {
        print('
        <tr>');
    }
    print('
            <td ' . ($objFuteli->isThemeActive($intThemeId) ? 'class="active"' : 'class="inactive"') . '>' . $arrThemeInfo['theme'] . '</td>
            <td ' . ($objFuteli->isThemeActive($intThemeId) ? 'class="active"' : 'class="inactive"') . '>' . $objFuteli->getThemePairedCount($intThemeId) . '</td>
            <td>
                <form accept-charset="UTF-8" method="POST" action="action.php">
                <input type="hidden" name="themeid" value="' . $intThemeId . '" />');
    if ($objFuteli->isThemeActive($intThemeId)) {
        print('
                <input type="submit" name="submit" value="Deaktiver" />');
    }
    else {
        print('
                <input type="submit" name="submit" value="Aktiver" />');
    }
    print('
                </form>
            </td>');
    if ($intLoopCount % 3 == 0) {
        print('
        </tr>');
    }
}
// Add </tr> if it wasn't added in the loop above.
if ($intLoopCount % 3 != 0) {
    print('
        </tr>');
}
print('
        <tr>
            <td colspan="6">
                <form accept-charset="UTF-8" method="POST" action="action.php">
                <input type="text" name="themename" size="25">
                <input type="submit" name="submit" value="Legg til nytt tema" />
                </form>
            </td>
        </tr>
    </tbody>
</table>');

print("
</body>");
