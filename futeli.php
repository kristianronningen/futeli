<?php

class Futeli {
    protected $arrEvents;
    // protected $arrEventPairings;
    protected $arrParticipants;         // *all* participants
    protected $arrActiveParticipants;   // only active participants
    protected $arrThemes;               // *all* themes
    protected $arrActiveThemes;         // only active themes

    // The maximum number of events to fetch
    protected $intFetchEventLimit = 50;

    protected $mysqli;

    public function __construct($strDbHost = 'localhost', $strDbUser = '', $strDbPass = '', $strDbName = 'futeli') {
        mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
        $this->mysqli = new mysqli($strDbHost, $strDbUser, $strDbPass, $strDbName);
        $this->mysqli->set_charset("utf8mb4");

        $this->loadParticipants();
        $this->loadActiveParticipants();
        $this->loadThemes();
        $this->loadActiveThemes();
    }

    public function loadParticipants() {
        $result = $this->mysqli->query("SELECT * FROM futeli_participants ORDER BY name");
        while ($row = $result->fetch_assoc()) {
            $this->arrParticipants[$row['participantid']]['name'] = $row['name'];
            $this->arrParticipants[$row['participantid']]['active'] = $row['active'];
        }
    }

    public function getParticipants() {
        return $this->arrParticipants;
    }

    public function loadActiveParticipants() {
        $result = $this->mysqli->query("SELECT participantid, name FROM futeli_participants WHERE active = 'Y'");
        while ($row = $result->fetch_assoc()) {
            $this->arrActiveParticipants[$row['participantid']] = $row['name'];
        }
    }

    public function getParticipantCount() {
        return count($this->arrParticipants);
    }

    public function getActiveParticipantCount() {
        return count($this->arrActiveParticipants);
    }

    public function getInactiveParticipantCount() {
        return count($this->arrParticipants) - count($this->arrActiveParticipants);
    }

    public function getParticipantName($id) {
        return $this->arrParticipants[$id]['name'];
    }

    // Number of pairings a participantid has
    public function getParticipantPairingCount($id) {
        $prep = $this->mysqli->prepare("SELECT count(1) FROM futeli_pairings WHERE participantid = ?");
        $prep->bind_param("i", $id);
        $prep->execute();
        $result = $prep->get_result();
        // Will only ever be one row
        $row = $result->fetch_row();
        return $row[0];
    }

    public function isParticipantActive($id) {
        if ($this->arrParticipants[$id]['active'] == 'Y') {
            return true;
        }
        else {
            return false;
        }
    }

    public function deactivateParticipant($id) {
        $prep = $this->mysqli->prepare("UPDATE futeli_participants SET active = 'N' WHERE participantid = ? LIMIT 1");
        $prep->bind_param("i", $id);
        $prep->execute();
    }

    public function activateParticipant($id) {
        $prep = $this->mysqli->prepare("UPDATE futeli_participants SET active = 'Y' WHERE participantid = ? LIMIT 1");
        $prep->bind_param("i", $id);
        $prep->execute();
    }

    public function addParticipant($strName) {
        $prep = $this->mysqli->prepare("INSERT INTO futeli_participants (name, active) VALUES (?, 'Y')");
        $prep->bind_param("s", $strName);
        $prep->execute();
    }

    public function loadThemes() {
        $result = $this->mysqli->query("SELECT * FROM futeli_themes ORDER BY theme");
        while ($row = $result->fetch_assoc()) {
            $this->arrThemes[$row['themeid']]['theme'] = $row['theme'];
            $this->arrThemes[$row['themeid']]['active'] = $row['active'];
        }
    }

    public function getThemes() {
        return $this->arrThemes;
    }

    public function loadActiveThemes() {
        $result = $this->mysqli->query("SELECT themeid, theme FROM futeli_themes WHERE active = 'Y'");
        while ($row = $result->fetch_assoc()) {
            $this->arrActiveThemes[$row['themeid']] = $row['theme'];
        }
    }

    public function getThemeCount() {
        return count($this->arrThemes);
    }

    public function getActiveThemeCount() {
        return count($this->arrActiveThemes);
    }

    public function getInactiveThemeCount() {
        return count($this->arrThemes) - count($this->arrActiveThemes);
    }

    public function getThemeName($id) {
        return $this->arrThemes[$id]['theme'];
    }

    // Number of times a themeid has been paired with a participant
    public function getThemePairedCount($id) {
        $prep = $this->mysqli->prepare("SELECT count(1) FROM futeli_pairings WHERE themeid = ?");
        $prep->bind_param("i", $id);
        $prep->execute();
        $result = $prep->get_result();
        // Will only ever be one row
        $row = $result->fetch_row();
        return $row[0];
    }

    public function isThemeActive($id) {
        if ($this->arrThemes[$id]['active'] == 'Y') {
            return true;
        }
        else {
            return false;
        }
    }

    public function deactivateTheme($id) {
        $prep = $this->mysqli->prepare("UPDATE futeli_themes SET active = 'N' WHERE themeid = ? LIMIT 1");
        $prep->bind_param("i", $id);
        $prep->execute();
    }

    public function activateTheme($id) {
        $prep = $this->mysqli->prepare("UPDATE futeli_themes SET active = 'Y' WHERE themeid = ? LIMIT 1");
        $prep->bind_param("i", $id);
        $prep->execute();
    }

    public function addTheme($strName) {
        $prep = $this->mysqli->prepare("INSERT INTO futeli_themes (theme, active) VALUES (?, 'Y')");
        $prep->bind_param("s", $strName);
        $prep->execute();
    }

    public function setFetchEventLimt($intLimit) {
        $this->intFetchEventLimit = $intLimit;
    }

    public function createEvent($strEventName) {
        // Make random pairings
        // $arrActiveParticipantThemes = array_rand($this->arrActiveThemes, count($this->arrActiveParticipants));
        // shuffle($arrActiveParticipantThemes);

        // print("<pre>");
        // print_r($arrActiveParticipantThemes);
        // print("</pre>");

        // Save event (unlocked)
        $prep = $this->mysqli->prepare("INSERT INTO futeli_events (created, name) VALUES ('" . date('Y-m-d H:i:s') . "', ?)");
        $prep->bind_param("s", $_POST['newEventName']);
        $prep->execute();
        $intEventId = $prep->insert_id;

        // $resEvent = mysqli_query($dbLink, "INSERT INTO futeli_events (created, name) VALUES ('" . date('Y-m-d H:i:s') . "', '" . $_POST['newEventName'] . "')");
        // $intEventId = mysqli_insert_id($dbLink);

        // Create and save participant<->theme pairings for the new event
        foreach ($this->arrActiveParticipants as $key => $intParticipantId) {
            $this->pickRandomSingleTheme($intEventId, $key);
            // $prep = $this->mysqli->prepare("INSERT INTO futeli_pairings (eventid, participantid, themeid) VALUES (?, ?, ?)");
            // $prep->bind_param("iii", $intEventId, $intParticipantId, $arrActiveParticipantThemes[$key]);
            // $prep->execute();
            // $resPair = mysqli_query($dbLink, "INSERT INTO futeli_pairings (eventid, participantid, themeid) VALUES (" . $intEventId . ", " . $intParticipantId . ", " . $arrParticipantThemes[$key] . ")");
        }
    }

    public function getEvents() {
        $prep = $this->mysqli->prepare("SELECT * FROM futeli_events ORDER BY created DESC LIMIT ?");
        $prep->bind_param("i", $this->intFetchEventLimit);
        $prep->execute();
        $result = $prep->get_result();
        while ($row = $result->fetch_assoc()) {
            $this->arrEvents[$row['eventid']]['created'] = $row['created'];
            $this->arrEvents[$row['eventid']]['name'] = $row['name'];
            $this->arrEvents[$row['eventid']]['locked'] = $row['locked'];
        }
        return $this->arrEvents;
    }

    public function lockEvent($intEventId) {
        $prep = $this->mysqli->prepare("UPDATE futeli_events SET locked = 'Y' WHERE eventid = ? LIMIT 1");
        $prep->bind_param("i", $intEventId);
        $prep->execute();
        // printf("%d row inserted.\n", $prep->affected_rows);
    }

    public function unlockEvent($intEventId) {
        $prep = $this->mysqli->prepare("UPDATE futeli_events SET locked = 'N' WHERE eventid = ? LIMIT 1");
        $prep->bind_param("i", $intEventId);
        $prep->execute();
    }

    public function isEventLocked($intEventId) {
        if ($this->arrEvents[$intEventId]['locked'] == 'Y') {
            return true;
        }
        else {
            return false;
        }
    }

    public function lockedStyle($intEventId) {
        if ($this->isEventLocked($intEventId) === true) {
            return ' class="locked"';
        }
        else {
            return ' class="unlocked"';
        }
    }

    // Delete the event and all pairings
    public function deleteEvent($intEventId) {
        $prep = $this->mysqli->prepare("DELETE FROM futeli_pairings WHERE eventid = ?");
        $prep->bind_param("i", $intEventId);
        $prep->execute();

        $prep = $this->mysqli->prepare("DELETE FROM futeli_events WHERE eventid = ?");
        $prep->bind_param("i", $intEventId);
        $prep->execute();
    }

    public function getEventPairings($intEventId) {
        // $prep = $this->mysqli->prepare("SELECT * FROM futeli_pairings WHERE eventid = ?");
        $prep = $this->mysqli->prepare("SELECT pair.eventid, pair.participantid, pair.themeid
                                        FROM futeli_pairings pair, futeli_participants part
                                        WHERE pair.eventid = ? AND pair.participantid = part.participantid
                                        ORDER BY part.name");
        $prep->bind_param("i", $intEventId);
        $prep->execute();
        $result = $prep->get_result();
        while ($row = $result->fetch_assoc()) {
            $arrEventPairings[$row['participantid']] = $row['themeid'];
        }
        return $arrEventPairings;
    }

    // Pick a (new) random theme for a single participant for a given event.
    // Make sure it's not a duplicate in the event, and one that the
    // participant hasn't had before.
    public function pickRandomSingleTheme($intEventId, $intParticipantId) {
        // $arrActiveThemes[id][theme|active]
        do {
            $newKey = array_rand($this->arrActiveThemes, 1);
        } while ($this->isDuplicateTheme($intEventId, $newKey) === true || $this->isParticipantDuplicateTheme($intParticipantId, $newKey) === true);
        $prep = $this->mysqli->prepare("REPLACE INTO futeli_pairings (eventid, participantid, themeid) VALUES (?, ?, ?)");
        $prep->bind_param("iii", $intEventId, $intParticipantId, $newKey);
        $prep->execute();
    }

    public function isDuplicateTheme($intEventId, $intThemeId) {
        $prep = $this->mysqli->prepare("SELECT * FROM futeli_pairings WHERE eventid = ? AND themeid = ?");
        $prep->bind_param("ii", $intEventId, $intThemeId);
        $prep->execute();
        $prep->store_result();
        // @file_put_contents("/home/steinbru/utfordringen-debug.log", "isDuplicateTheme: " . $prep->num_rows . "\n", FILE_APPEND);
        if ($prep->num_rows > 0) {
            // Found an event<->themeid pairing already, is a duplicate
            return true;
        }
        else {
            return false;
        }
    }

    // TODO: Add a time factor to this duplicate check?
    public function isParticipantDuplicateTheme($intParticipantId, $intThemeId) {
        $prep = $this->mysqli->prepare("SELECT * FROM futeli_pairings WHERE participantid = ? AND themeid = ?");
        $prep->bind_param("ii", $intParticipantId, $intThemeId);
        $prep->execute();
        $prep->store_result();
        // @file_put_contents("/home/steinbru/utfordringen-debug.log", "isParticipantDuplicateTheme: " . $prep->num_rows . "\n", FILE_APPEND);
        if ($prep->num_rows > 0) {
            return true;
        }
        else {
            return false;
        }
    }
}
