CREATE TABLE `futeli_events` (
  `eventid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `locked` enum('N','Y') NOT NULL DEFAULT 'N',
  PRIMARY KEY (`eventid`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

CREATE TABLE `futeli_pairings` (
  `eventid` smallint(5) unsigned NOT NULL,
  `participantid` smallint(5) unsigned NOT NULL,
  `themeid` smallint(5) unsigned NOT NULL,
  UNIQUE KEY `eventparticipant` (`eventid`,`participantid`),
  KEY `themeid` (`themeid`),
  KEY `participantid` (`participantid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

CREATE TABLE `futeli_participants` (
  `participantid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `active` enum('N','Y') NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`participantid`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

CREATE TABLE `futeli_themes` (
  `themeid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `theme` varchar(50) NOT NULL,
  `active` enum('N','Y') NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`themeid`),
  UNIQUE KEY `theme` (`theme`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
