<?php

require_once('config.php');
require_once('futeli.php');

print("<!DOCTYPE html>
<html>
<head>
    <meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\" />
    <link rel=\"stylesheet\" type=\"text/css\" href=\"futeli.css\" />
    <title>Utfordringen - tilfeldige temaer</title>
</head>
<body>");

// Show link to admin page
print('
<div class="lagny">
    <a href="admin/">Administrering</a>
</div>
');

$objFuteli = new Futeli($strDbHost, $strDbUser, $strDbPass, $strDbName);
$arrEvents = $objFuteli->getEvents();

foreach ($arrEvents as $eventId => $arrEventInfo) {
    // For each event, fetch the pairings (participant<->theme)
    $arrEventPairings[$eventId] = $objFuteli->getEventPairings($eventId);
}

// Build a table of the events, participants and themes
print('
<table border="0">
    <thead>
    <tr>');
// Header, one column for each event
foreach ($arrEvents as $eventId => $arrEventInfo) {
    // Convert the draw date to a more human readable form
    $intTimestamp = strtotime($arrEventInfo['created']);
    $fmt = datefmt_create('nb_NO.UTF-8', IntlDateFormatter::FULL, IntlDateFormatter::FULL, 'Europe/Oslo', IntlDateFormatter::GREGORIAN  ,"d. LLL yyyy");
    print('
        <th' . $objFuteli->lockedStyle($eventId) . '>' . $arrEventInfo['name'] . '<br />
            <span class="trukket">Trukket: ' . datefmt_format($fmt, $intTimestamp) . '</span>
        </th>');
}
print("
    </tr>
    </thead>
    <tbody>
    <tr>");
// @file_put_contents("/home/steinbru/utfordringen-debug.log", "arrEventPairings: " . var_export($arrEventPairings, true) . "\n", FILE_APPEND);
foreach($arrEventPairings as $eventId => $arrPairingInfo) {
    print('
        <td valign="top"' . $objFuteli->lockedStyle($eventId) . '>
            <table>
            <tbody>');
    foreach ($arrPairingInfo as $participantId => $themeId) {
        if ($objFuteli->isParticipantActive($participantId)) {
            print('
                    <tr>
                        <td>' . $objFuteli->getParticipantName($participantId) . "</td>
                        <td>" . $objFuteli->getThemeName($themeId) . "</td>
                    </tr>");
        }
    }
    print("
            </tbody>
            </table>
        </td>");
}

print("
    </tr>
    </tbody>
</table>
</body>");
