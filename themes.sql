INSERT INTO `futeli_themes` (`theme`) VALUES
('Arkitektur'),('Bekymret'),('Blomst i naturen'),('På bygda / bygdeliv'),('Bygning(er)'),
('Byliv'),('Delt / dele'),('Dyr'),('Dårlig vær'),('Farge: Blå'),('Farge: Grønn'),('Farge: Gul'),
('Farge: Rød'),('Fargeløst (blast)'),('Fargerikt'),('Fart'),('Fjell/fjellet'),('Folk i farta'),
('Fritt motiv - farge'),('Fritt motiv - monokrom'),('Frost'),('Fuktig'),('Følelser'),('Gate/vei'),
('Glad'),('Glimt'),('Hagen'),('Highkey'),('Humor'),('Hus(et)'),('Himmel'),('Sky/skyer'),
('I farta'),('I gata/veien'),('Igjennom'),('Ingeniørkunst'),('Kolleksjon'),('Kontrast'),('Kveld'),
('Fritt motiv'),('Lek'),('Lettet'),('Linjer'),('Liten dybdeskarphet'),('Lowkey'),('Lykke'),
('Lys'),('Lyspunkt'),('Menneske(r) og dyr'),('Mennesker (flertall)'),('Mennesket (entall)'),
('Metall'),('Minimalisme'),('Mørke'),('Natt'),('Norsk kultur'),('Panorering'),('Plante(r)'),
('På kryss og tvers'),('På vei'),('Redd'),('Refleksjon'),('Alderdom / aldring'),('Ungdom'),
('Barn'),('Barnlig lek'),('Samspill'),('Speiling'),('Stein(er)'),('Stemning'),('Symmetri'),
('Trangt'),('Tre / trær'),('Treverk'),('Trist'),('Vann'),('Soloppgang'),('Solnedgang'),('Slottet'),
('Akershus festning'),('Aker Brygge'),('Karl Johan'),('Oslo S'),('Tog'),('Spor'),('Buss'),
('Trikken'),('Shopping'),('Avslappet'),('Portrett'),('Urban kunst'),('Sørenga'),('Operaen'),
('Sort/hvitt'),('Nytt & gammelt'),('Landskap, by'),('Kulturlandskap'),('Landskap med vann'),
('På fjellet'),('I skogen'),('På / ved sjøen'),('Hjul'),('Gammel bil'),('Blank / blankt'),
('Refleks'),('Slitt / sliten'),('Lykkelig'),('Skjev / skakk'),('Hår / hårete'),('Dans / danse'),
('Ensom / ensomhet'),('Forfalt /forfall'),('Hånd / Hender'),('Kropp'),('Levende lys'),
('Makro / nærfoto'),('Over vann'),('Under vann'),('Selfie (mobil)'),('Selvportrett (systemkamera)'),
('Kjærlighet'),('Hat'),('Miniatyr (figur) foto'),('Utsikt'),('Rund'),('Firkant'),('Varme'),
('Vennskap'),('Stilleben'),('Hatt'),('Galskap'),('Aktfoto / nude art'),('Smak av honning'),
('Søppel og skrap'),('Avisen'),('Et glass vin'),('Familie');
